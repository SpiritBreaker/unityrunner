﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleAnimation : MonoBehaviour {

    public Spline.SplinePoint point;
    public float height = 2f;
    public float size = 1.0f;
    public float rotate = 0.0f;

	// Update is called once per frame
	void Update () {

	}
}
