﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Spline;

public class Game : MonoBehaviour {


    #region Singleton
    private static Game _instance;
    public static Game Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Game>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("Game");
                    _instance = container.AddComponent<Game>();
                }
            }

            return _instance;
        }
    }
    #endregion


    #region CompositionRoot


    public static PathHolder path_holder;
    public static IApproximation approximation;
    public static ISpline spline;


    #endregion

    public delegate void GameStateEvent();
    public static event GameStateEvent gameLoadNotification;
    public static event GameStateEvent gameOverNotification;
    public static event GameStateEvent gamePauseNotification;
    public static event GameStateEvent gameUnpauseNotification;
    public static event GameStateEvent gameStartNotification;
    public static event GameStateEvent gameQuitNotification;

    enum GameStates { IDLE, PAUSE, PLAY };

    GameStates GameState = GameStates.IDLE;


    void Awake()
    {
        approximation = new NumericApproximation();
        path_holder = GetComponent<PathHolder>();
        spline = new CatmullRom(approximation, path_holder);
    }

    void Start()
    {
        gameLoad();
        Player.livesChangeNotification += PlayerLivesChanged;
        PlayButton.PlayButtonPushed += gameStart;
        PauseButton.PauseButtonPushed += gamePause;
        BackButton.BackButtonPushed += gameUnpause;
        RestartButton.RestartButtonPushed += gameUnpause;
        RestartButton.RestartButtonPushed += gameRestart;

    }

    public void PlayerLivesChanged(int lives)
    {
        if (lives < 1)
        {
            gameOver();
        }
    }

    public void PlayerSpeedChanged(float speed)
    {
    }

    public void gameLoad()
    {
        gameLoadNotification();
    }

    public void gameQuit()
    {
        gameQuitNotification();
    }

    public void gameStart()
    {
        gameStartNotification();
    }

    public void gameOver()
    {
        gameOverNotification();
    }

    public void gamePause()
    {
        gamePauseNotification();
    }

    public void gameUnpause()
    {
        gameUnpauseNotification();
    }

    public void gameRestart()
    {
        gameOverNotification();
        gameStartNotification();

    }
	
	// Update is called once per frame
	void Update () 
    {
	}
}
