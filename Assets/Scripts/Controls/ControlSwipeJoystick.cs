﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSwipeJoystick : IControl {

	
	private bool tap, swipeLeft, swipeRight, swipeUp, swipeDown;
	private bool isDraging = false;
	private Vector2 startTouch, swipeDelta;
	private float swipeLength;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		tap = swipeLeft = swipeRight = swipeUp = swipeDown = false;
		if (Input.GetMouseButtonDown(0))
		{
			tap = true;
			acceleration.x = 0f;
			isDraging = true;
			startTouch = Input.mousePosition;
		}
		
		else if(Input.GetMouseButtonUp(0))
		{
			isDraging = false;
			acceleration.x = 0f;
			Reset();
		}


		if (Input.touches.Length > 0)
		{
			if (Input.touches[0].phase == TouchPhase.Began)
			{
				tap = true;
				acceleration.x = 0f;
				isDraging = true;
				startTouch = Input.touches[0].position;
			}
			else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
			{
				isDraging = false;
				Reset();
			}			
		}


		//Calculate the distance
		swipeDelta = Vector2.zero;
		if (isDraging)
		{
			if (Input.touches.Length > 0)
			{
				swipeDelta = Input.touches[0].position - startTouch;
			}
			else if (Input.GetMouseButton(0))
			{
				swipeDelta = (Vector2)Input.mousePosition - startTouch;
			}
		}

		//Did we cross the deadzone
		if(swipeDelta.magnitude > 10)
		{
			float x = swipeDelta.x;
			float y = swipeDelta.y;
			if (Mathf.Abs(x) > Mathf.Abs(y))
			{
				// left or right
				if (x < 0)
				{
					swipeLeft = true;
					acceleration.x = -1f;// * (swipeDelta.magnitude/100f);
				}
				else
				{
					swipeRight = true;
					acceleration.x = 1f;// * (swipeDelta.magnitude/100f);
				}
			}
			else
			{
				// up or down
				if (y < 0)
				{
					swipeDown = true;
				}
				else
				{
					swipeUp = true;
				}
			}
			//Reset();
		}
	}

	private void Reset()
	{
		startTouch = swipeDelta = Vector2.zero;
		isDraging = false;
	}

}
