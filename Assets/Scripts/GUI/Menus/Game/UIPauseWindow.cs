﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPauseWindow : UIObject
{

    public void Awake()
    {
        PauseButton.PauseButtonPushed += showElements;
        BackButton.BackButtonPushed += hideElements;
        RestartButton.RestartButtonPushed += hideElements;
        base.Awake();
    }

    public Canvas[] canvases;

    private void hideElements()
    {
        foreach (Canvas canvas in canvases)
        {
            canvas.enabled = false;
        }
    }
    private void showElements()
    {
        foreach (Canvas canvas in canvases)
        {
            canvas.enabled = true;
        }
    }

    protected override void gameStart()
    {
        hideElements();
    }

    protected override void gamePause()
    {
        showElements();
    }

    protected override void gameLoad()
    {
        hideElements();
    }

    protected override void gameOver()
    {
        hideElements();
    }

    protected override void gameUnpause()
    {
        hideElements();
    }
}
