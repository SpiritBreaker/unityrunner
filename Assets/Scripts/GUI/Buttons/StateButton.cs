﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateButton : SimpleButton {

    public Sprite[] states;
    protected int state = 0;

    public delegate void State(int state);

    public void Start()
    {
        this.gameObject.GetComponent<Image>().sprite = states[state];
    }

    public virtual void StateChange()
    {
        state = (state + 1) % states.Length;
        this.gameObject.GetComponent<Image>().sprite = states[state];
    }
}
