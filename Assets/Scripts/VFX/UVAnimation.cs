﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVAnimation : MonoBehaviour {

    public float animationSpeed = 5F;
    private Renderer rend; 

	// Use this for initialization
	void Start () {
		 rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        float offset = Time.time * animationSpeed;
        rend.material.SetTextureOffset("_MainTex", new Vector2(-offset, rend.material.GetTextureOffset("_MainTex").y));
	}
}
