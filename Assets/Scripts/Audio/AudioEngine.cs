﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AudioEngine : MonoBehaviour {

   	#region Singleton
    private static AudioEngine _instance;
    public static AudioEngine Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<AudioEngine>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("Player");
                    _instance = container.AddComponent<AudioEngine>();
                }
            }

            return _instance;
        }
    }
    #endregion

	public AudioSource ObstacleAudioSource;
	public AudioSource RewardsAudioSource;
	public AudioSource CommonAudioSource;	

}
