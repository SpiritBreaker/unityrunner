﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    private bool mouseControl = true;
    private float rotationSpeed;
    public float angSpeed;
    public static float sensivity  = 0.1f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        this.rotationSpeed = (float)(-((double)Input.mousePosition.x - (double)Screen.width / 2.0) * 0.00999999977648258) * Mathf.Lerp(0.7f, 2f, sensivity);
        if ((double)Time.timeScale < 1.0)
            this.rotationSpeed = this.rotationSpeed * Time.timeScale;
        this.angSpeed = this.rotationSpeed * 60f * Time.deltaTime;
        this.transform.Rotate(new Vector3(0.0f, 0.0f, this.angSpeed));

    }
}
