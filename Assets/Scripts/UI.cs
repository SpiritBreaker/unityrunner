﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : InGameObject
{
    public Text scoreText;
    public Text livesText;
    public Text coinsText;


    // Use this for initialization
    void Start()
    {
        livesText.text = "Lives: " + Player.Instance.lives;
        Player.livesChangeNotification += update_lives;
        base.Start();
    }

    void update_lives(int lives)
    {
        livesText.text = "Lives: " + lives;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + (int)Player.Instance.traveledDistance;
        livesText.text = "Lives: " + Player.Instance.lives;
        coinsText.text = "" + Player.Instance.coins;
    }

}
