﻿using UnityEngine;
using System.Collections;
using Spline;

//http://www.habrador.com/tutorials/interpolation/1-catmull-rom-splines/




public class MoveOnPathScript : InGameObject
{
    IApproximation NumericApproximation;
    public ISpline Spline;

    public Camera camera;
    public GameObject aim_object;
    public GameObject pathWalker;
    public float up_vector = 1.0f;

    public IControl Control;

    public float angle = 0;

    Vector3 p0, p1, p2, p3;
    Vector3 c0, c1, c2, c3;

    Vector3 tangent, normal, binormal;
 
    float t = 0.0f;
    int previous_t = -1;

    private float rotation = 0.0f;

    private SplinePoint currentPos;
    private SplinePoint lastPos;


    public float acceleration;


    void Start()
    {
        base.Start();
        t = Player.Instance.position;
        this.angle = Player.Instance.angle;
        Spline = Game.spline;

        currentPos = Spline.get_point((int)t, t %1 );
        lastPos = currentPos;

        Control = gameObject.AddComponent<LegacyControlSwipe>();

    }

    void get_p_c()
    {
        if ((int)t > Spline.amount_of_points - 1)
        {
            t = 0.0f;
        }

        p0 = Spline.get_knot(  (int)MyMath.mod((int)t - 1, Spline.amount_of_points)  ).position;
        p1 = Spline.get_knot(  (int)MyMath.mod((int)t,     Spline.amount_of_points)  ).position;
        p2 = Spline.get_knot(  (int)MyMath.mod((int)t + 1, Spline.amount_of_points)  ).position;
        p3 = Spline.get_knot(  (int)MyMath.mod((int)t + 2, Spline.amount_of_points)  ).position;

        c0 = Spline.get_knot(  (int)MyMath.mod((int)t - 2, Spline.amount_of_points)  ).position;
        c1 = Spline.get_knot(  (int)MyMath.mod((int)t - 1, Spline.amount_of_points)  ).position;
        c2 = Spline.get_knot(  (int)MyMath.mod((int)t,     Spline.amount_of_points)  ).position;
        c3 = Spline.get_knot(  (int)MyMath.mod((int)t + 1, Spline.amount_of_points)  ).position;

    }

    // Update is called once per frame
    void Update()
    {
        t += Player.Instance.speed * Time.deltaTime;

        if ((int)t != previous_t)
        {
            previous_t = (int)t;
            get_p_c();
        }

        Player.Instance.position = (int)t;
        Player.Instance.segment_position = t % 1;
        currentPos = Spline.get_point((int)t, t %1 );
        Player.Instance.traveledDistance += Vector3.Distance(currentPos.position, lastPos.position);
        lastPos = currentPos;

        tangent = Spline.get_tangent(t % 1, p0, p1, p2, p3).normalized;
        normal = Vector3.Cross(tangent, aim_object.transform.up).normalized;
        binormal = Vector3.Cross(normal, tangent).normalized;
        tangent = Vector3.Cross(binormal, normal).normalized;

        aim_object.transform.position = Spline.get_world_position(t % 1, p0, p1, p2, p3) + (aim_object.transform.up * Player.Instance.height);

        //Z rotations are transformations are 
        aim_object.transform.forward = tangent;

        aim_object.transform.Rotate(tangent, Control.acceleration.angle, Space.World);
        this.angle = aim_object.transform.rotation.eulerAngles.z;
        this.acceleration = Control.acceleration.output;
      
        Player.Instance.angle = this.angle;


        camera.transform.position = Spline.get_world_position(t % 1, c0, c1, c2, c3) + (camera.transform.up * Player.Instance.height);
        camera.transform.LookAt(aim_object.transform.position, aim_object.transform.up);
    }
}