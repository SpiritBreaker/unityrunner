﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reward : InteractiveObject {

    void Start()
    {
        base.Start();
    }

    protected void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("player"))
        {
            poolManager.Despawn(this.gameObject);
        }
        if(other.gameObject.CompareTag("culling_plane"))
        {
            poolManager.Despawn(this.gameObject);
        }
    }
}
