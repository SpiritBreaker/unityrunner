﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spline;

public abstract class IProductionLine : MonoBehaviour   {

	public float length {get; set;}
	public Vector3 startVector {get; set;}
	public Vector3 endVector {get; set;}
	public float density {get; set;}

	public abstract GameObject Create(PoolManager poolManager, ISpline spline, double begin, double end);
}

