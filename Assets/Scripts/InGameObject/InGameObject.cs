﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InGameObject : MonoBehaviour
{

    public bool subsribed = false;

    private void subscribe()
    {
        Game.gameOverNotification += gameOver;
        Game.gameStartNotification += gameStart;
        Game.gamePauseNotification += gamePause;
        Game.gameUnpauseNotification += gameUnpause;
        Game.gameLoadNotification += gameLoad;
        subsribed = true;
    }

    private void unsubsribe()
    {
        Game.gameOverNotification -= gameOver;
        Game.gameStartNotification -= gameStart;
        Game.gamePauseNotification -= gamePause;
        Game.gameUnpauseNotification -= gameUnpause;
        Game.gameLoadNotification -= gameLoad;
        subsribed = false;
    }

    protected void Awake()
    {
        if (isActiveAndEnabled)
        {
            subscribe();
        }
    }

    protected void Start()
    {
    }

    protected virtual void  gameOver()
    {
    }
    protected virtual void gameStart()
    {
    }

    protected virtual void gamePause()
    {
        foreach (MonoBehaviour script in gameObject.GetComponents<MonoBehaviour>())
        {
            script.enabled = false;
        }
    }

    protected virtual void gameUnpause()
    {
        foreach (MonoBehaviour script in gameObject.GetComponents<MonoBehaviour>())
        {
            script.enabled = true;
        }
    }
    protected virtual void gameLoad()
    {
    }

    void onDisabled()
    {
        unsubsribe();
    }

    void onActive()
    {
        subscribe();

    }
    void OnDestroy()
    {
        if (subsribed)
        {
            unsubsribe();
        }
    }
}
