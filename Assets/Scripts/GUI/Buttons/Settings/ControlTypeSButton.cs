﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlTypeSButton : StateButton
{
    public static event State ControlTypeStateChanged;

    public void Awake()
    {
        state = Account.Instance.controls_type;
    }

    public override void StateChange()
    {
        base.StateChange();
        if (ControlTypeStateChanged != null)
        {
            ControlTypeStateChanged(state);
        }
    }
}
