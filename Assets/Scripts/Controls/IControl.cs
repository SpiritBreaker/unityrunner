﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Acceleration
{
	public float x {get; set;}
	public float y {get; set;}
	public float z {get; set;}
	public float angle{get; set;}
	public float output{get;set;}
}

public struct SwipeContainer
{
	public Vector3 startVector;
	public Vector3 endVector;
	public float angle;
	public float time; 
}

public abstract class IControl : MonoBehaviour
{
	protected Acceleration _acceleration;
	public Acceleration acceleration 
	{ 
		get
		{
			if (_acceleration == null)
			{
				_acceleration = new Acceleration();
			}

			return _acceleration;
		} 
		set
		{

		} 
	}
}
