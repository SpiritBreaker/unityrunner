﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerkLive : Perk {

	
	// Update is called once per frame
	void Update () 
    {
	}

    protected void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("player"))
        {
            Player.Instance.lives +=1;	
        }
        base.OnCollisionEnter(other);
    }

}
