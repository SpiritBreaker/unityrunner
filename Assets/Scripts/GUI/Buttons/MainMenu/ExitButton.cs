﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButton : SimpleButton
{

    public static event ButtonPushed QuitButtonPressed;

    public override void OnPointerDown()
    {
        if (QuitButtonPressed != null)
        {
            QuitButtonPressed();
        }

        Application.Quit(); 
    }
}
