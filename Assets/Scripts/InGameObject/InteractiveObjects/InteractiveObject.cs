﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObject : InGameObject
{
    [SerializeField]
    private PoolManager _poolManager;
    [SerializeField]
    private int _durability = 0;
    [SerializeField]
    private int _spline_point = 0;
    [SerializeField]
    private Spline.SplinePoint _point;
    [SerializeField]
    private float _height = 2f;
    [SerializeField]
    private float _angle = 0.0f;
    [SerializeField]
    private float _angle_speed = 0.0f;
    [SerializeField]
    private float _size = 1.0f;
    [SerializeField]
    private bool _triggered = false;

    [SerializeField]
    private int _height_animation = 0;
    [SerializeField]
    private int _angle_animation = 0;
    [SerializeField]
    private int _size_animation = 0;

    [SerializeField]
    private bool _destroyOnFinish = false;

    public Spline.SplinePoint point 
    { 
        get 
        { 
            return _point; 
        }
        set
        { 
            _point = value;
        } 
    }

    [SerializeField]
    private AudioClip _onEnterSound;
    [SerializeField]
    private AudioClip _remainingSound;
    [SerializeField]
    private AudioClip _leavingSound;
    
    [SerializeField]
    private GameObject _causingVFX;
    [SerializeField]
    private GameObject _remainingVFX;
    [SerializeField]
    private GameObject _leavingVFX;

    public AudioClip onEnterSound { get { return _onEnterSound; } set { _onEnterSound = value; } }
    public AudioClip remainingSound { get { return _remainingSound; } set { _remainingSound = value; } }
    public AudioClip leavingSound { get { return _leavingSound; } set { _leavingSound = value; } } 
    

    public GameObject causingVFX  { get { return _causingVFX; } set { _causingVFX = value; } }
    public GameObject remainingVFX  { get { return _remainingVFX; } set { _remainingVFX = value; } }
    public GameObject leavingVFX  { get { return _leavingVFX; } set { _leavingVFX = value; } }

    

    public PoolManager poolManager { get { return _poolManager; } set { _poolManager = value; } }

    public int durability { get { return _durability; } set { _durability = value; } }
    public float height { get { return _height; } set { _height = value; } }
    public float angle { get { return _angle; } set { _angle = value; } }
    public float angle_speed { get { return _angle_speed; } set { _angle_speed = value; } }
    public float size { get { return _size; } set { _size = value; } }
    public bool triggered { get { return _triggered; } set { _triggered = value; } }

    public int height_animation { get { return _height_animation; } set { _height_animation = value; } }
    public int angle_animation { get { return _angle_animation; } set { _angle_animation = value; } }
    public int size_animation { get { return _size_animation; } set { _size_animation = value; } }

    public int spline_point { get { return _spline_point; } set { _spline_point = value; } }

    public bool destroyOnFinish { get { return _destroyOnFinish; } set { _destroyOnFinish = value; } }


    // Use this for initialization
    public void Start()
    {
        base.Start();
        this.GetComponent<Collider>().isTrigger = triggered;
    }

    // Update is called once per frame
    protected void FixedUpdate()
    {
        this.transform.RotateAround(_point.position, this.transform.forward, angle_speed * Time.deltaTime * angle_animation);
    }

    protected void emmitSound(AudioClip clip, float volume)
    {
        AudioEngine.Instance.CommonAudioSource.PlayOneShot(clip, volume);
    }
}
