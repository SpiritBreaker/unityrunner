﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSButton : StateButton
{
    public static event State MusicStateChanged;

    public void Awake()
    {
        state = Account.Instance.music;
    }

    public override void StateChange()
    {
        base.StateChange();
        if (MusicStateChanged != null)
        {
            MusicStateChanged(state);
        }
    }
}
