﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spline;

public class ISplineDriven : InteractiveObject {

    public ISpline Spline;
    public float startPosition;
    [SerializeField]
	public float speed;
	public int direction;

    public int position;
    public float segment_position;

    float t = 0.0f;
    int previous_t = -1;

    Vector3 p0, p1, p2, p3;

    Vector3 tangent, normal, binormal;

    public void Start()
	{
        base.Start();
        //t = Player.Instance.position + 1;
        t = startPosition;
		Spline = Game.spline;
	}

    void get_p_c()
    {
        if ((int)t > Spline.amount_of_points - 1)
        {
            t = 0.0f;
        }

        p0 = Spline.get_knot(  (int)MyMath.mod((int)t - 1, Spline.amount_of_points)  ).position;
        p1 = Spline.get_knot(  (int)MyMath.mod((int)t,     Spline.amount_of_points)  ).position;
        p2 = Spline.get_knot(  (int)MyMath.mod((int)t + 1, Spline.amount_of_points)  ).position;
        p3 = Spline.get_knot(  (int)MyMath.mod((int)t + 2, Spline.amount_of_points)  ).position;
    }

    void FixedUpdate()
    {
        t += 2f * Time.deltaTime;

        if ((int)t != previous_t)
        {
            previous_t = (int)t;
            get_p_c();
        }

        this.position = (int)t;
        this.segment_position = t % 1;

        this.point = Spline.get_point((int)t, t % 1);
        tangent = Spline.get_tangent(t % 1, p0, p1, p2, p3).normalized;
        normal = Vector3.Cross(tangent, this.transform.up).normalized;
        binormal = Vector3.Cross(normal, tangent).normalized;
        tangent = Vector3.Cross(binormal, normal).normalized;

        this.transform.forward = tangent;
        this.transform.position = Spline.get_world_position(t % 1, p0, p1, p2, p3) + (this.transform.up * this.height);

        angle -= Time.deltaTime * angle_speed * angle_animation;
        angle = angle % 360;
        this.transform.RotateAround(this.point.position, this.transform.forward, angle);
    }

}
