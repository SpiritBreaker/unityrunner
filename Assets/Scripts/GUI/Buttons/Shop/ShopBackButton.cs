﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopBackButton : SimpleButton {

    public static event ButtonPushed ShopBackButtonPushed;

    // Use this for initialization
    public override void OnPointerDown()
    {
        ShopBackButtonPushed();
        base.OnPointerDown();
    }
}
