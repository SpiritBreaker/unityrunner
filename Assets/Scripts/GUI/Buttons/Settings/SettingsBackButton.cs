﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsBackButton : SimpleButton {

    public static event ButtonPushed SettingsBackButtonPushed;

	// Use this for initialization
    public override void OnPointerDown()
    {
        SettingsBackButtonPushed();
        base.OnPointerDown();
    }
}
