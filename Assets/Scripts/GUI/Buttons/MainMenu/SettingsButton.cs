﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsButton : SimpleButton
{
    public static event ButtonPushed SettingsButtonPushed;

    public override void OnPointerDown()
    {
        if (SettingsButtonPushed != null)
        {
            SettingsButtonPushed();
        }
    }
}
