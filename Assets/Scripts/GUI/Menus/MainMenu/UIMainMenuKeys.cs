﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainMenuKeys : UIObject
{
	// Use this for initialization

    public Canvas [] canvases;

    public Text bestScoresText;
    public Text simpleCoinsText;
    public Text extraLivesText;

    void Awake()
    {
        base.Awake();
        PlayButton.PlayButtonPushed += hideElements;

        SettingsButton.SettingsButtonPushed += hideElements;
        SettingsBackButton.SettingsBackButtonPushed += showElements;

        ShopButton.ShopButtonPushed += hideElements;
        ShopBackButton.ShopBackButtonPushed += showElements;

    }

    private void hideElements()
    {
        foreach (Canvas canvas in canvases)
        {
            canvas.enabled = false;
        }
    }
    private void showElements()
    {
        foreach (Canvas canvas in canvases)
        {
            canvas.enabled = true;
        }
    }

    protected override void gameStart()
    {
        hideElements();
    }

    protected override void gameOver()
    {
        showElements();
    }

    protected override void gameLoad()
    {
        bestScoresText.text = Account.Instance.bestScores.ToString();
        simpleCoinsText.text = Account.Instance.coins.ToString();
        showElements();
        base.gameLoad();
    }
}
