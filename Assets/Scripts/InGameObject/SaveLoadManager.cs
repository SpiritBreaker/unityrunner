﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.IO;

public class SaveLoadManager : InGameObject 
{
    #region Singleton
    private static SaveLoadManager _instance;
    public static SaveLoadManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<SaveLoadManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("SaveLoadManager");
                    _instance = container.AddComponent<SaveLoadManager>();
                }
            }

            return _instance;
        }
    }
    #endregion

    public void Start()
    {
        base.Start();
    }

    public void SaveAccount(Account account)
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream stream = new FileStream (Application.persistentDataPath + "/player.sav", FileMode.Create);
		AccountData data = new AccountData (account);
		bf.Serialize (stream, data);
		stream.Close ();
	}

	public Dictionary<string, int>  LoadPlayer()
	{
		if (File.Exists (Application.persistentDataPath + "/player.sav")) 
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream stream = new FileStream (Application.persistentDataPath + "/player.sav", FileMode.Open);
			AccountData data = (AccountData)bf.Deserialize(stream);
			stream.Close ();
			return data.stats;
		} 
        else
        {
            Debug.LogWarning("SaveFile does not exist");
			return new  Dictionary<string, int>();
	    }
    }

    protected override void gameOver()
    {
        Account.Instance.Save();
        base.gameOver();
    }

    protected override void gameLoad()
    {
        Account.Instance.Load();
        base.gameLoad();
    }

}
	
[Serializable]
public class AccountData
{
    public Dictionary<string, int> stats = new Dictionary<string, int>();
    public AccountData(Account account)
	{
        foreach (PropertyInfo property in typeof(Account).GetProperties(
            BindingFlags.Public | 
            BindingFlags.Instance |
            BindingFlags.DeclaredOnly))
        {
            stats[property.Name] = (int)property.GetValue(account, null);
        }
	}
}