﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spline;

public class SimpleProductionLine : IProductionLine {

	[SerializeField]
	public GameObject prefab;

	[SerializeField]
	public float destroyOnFinish;

	[SerializeField]
	public double step;
	
	[SerializeField]	
	public bool test = false;

	private PoolManager poolManager;

    List<GameObject> products = new List<GameObject>();


	public GameObject CreateSingleObject()
	{
		GameObject instance = poolManager.Spawn(prefab);
        InteractiveObject settings = instance.GetComponent<InteractiveObject>();
		settings.poolManager = poolManager;
        settings.destroyOnFinish = true;
		
		return instance;
    }

	public override GameObject Create(PoolManager poolManager, ISpline spline, double begin, double end)
	{
		this.poolManager = poolManager;

		double length = end - begin ;
		double point = begin;
		int number = (int)(length/step);

		float rotation = Random.Range(0, 360);

		for (int i = 0; i<number; i++)
		{
			GameObject newInstance = CreateSingleObject();
			InteractiveObject settings = newInstance.GetComponent<InteractiveObject>();
			settings.point = spline.DistanceToPoint(point);
			settings.angle = rotation;

			newInstance.transform.forward = settings.point.tangent;
        	newInstance.transform.position = settings.point.position + (newInstance.transform.up * settings.height);
        	newInstance.transform.RotateAround(settings.point.position, settings.point.tangent, settings.angle);

			point+=step;
			if(test)
				rotation +=25f;
		}
		return null;
	}

}
