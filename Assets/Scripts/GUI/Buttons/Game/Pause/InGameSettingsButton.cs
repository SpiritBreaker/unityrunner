﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameSettingsButton : SimpleButton
{
    public static event ButtonPushed InGameSettingsButtonPushed;
    public override void OnPointerDown()
    {
        if (InGameSettingsButtonPushed != null)
        {
            InGameSettingsButtonPushed();
        }
    }
}
