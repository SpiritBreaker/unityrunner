﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButton : SimpleButton
{
    public static event ButtonPushed PauseButtonPushed;
    public override void OnPointerDown()
    {
        if (PauseButtonPushed != null)
        {
            PauseButtonPushed();
        }
    }
}
