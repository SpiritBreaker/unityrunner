﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


public class Account : InGameObject {

    #region Singleton
    private static Account _instance;
    public static Account Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Account>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("Account");
                    _instance = container.AddComponent<Account>();
                }
            }
            return _instance;
        }
    }
    #endregion

    //gameplay
    [SerializeField]
    private int _extra_lives = 3;
    public int extra_lives { get { return _extra_lives; } set { _extra_lives = value; } }

    [SerializeField]
    private int _coins = 0;
    public int coins { get { return _coins; } set { _coins = value; } }

    [SerializeField]
    private int _bestScores = 0;
    public int bestScores { get { return _bestScores; } set { _bestScores = value; } }

    [SerializeField]
    private int _noAd = 0;
    public int noAd { get { return _noAd; } set { _noAd = value; } }

    //settings
    [SerializeField]
    private int _volume = 1;
    public int volume { get { return _volume; } set { _volume = value; } }

    [SerializeField]
    private int _music = 1;
    public int music { get { return _music; } set { _music = value; } }

    [SerializeField]
    private int _controls_type = 0;
    public int controls_type { get { return _controls_type; } set { _controls_type = value; } }

    [SerializeField]
    private int _test = 0;
    public int test { get { return _test; } set { _test = value; } }

	public void Save()
	{
		SaveLoadManager.Instance.SaveAccount(this);
	}

    public void Start()
    {
        VolumeSButton.VolumeStateChanged += (int state) => { volume = state; };
        ControlTypeSButton.ControlTypeStateChanged += (int state) => { controls_type = state; };
        MusicSButton.MusicStateChanged += (int state) => { music = state; };
        base.Start();
    }

	public void Load()
	{
        Dictionary<string, int> loadStats  = SaveLoadManager.Instance.LoadPlayer();
        foreach (KeyValuePair<string, int> entry in loadStats)
        {
            var property = this.GetType().GetProperty(entry.Key);
            int temp;
            loadStats.TryGetValue(entry.Key, out temp);
            if ( this.GetType().GetProperty(entry.Key) != null)
            {
                this.GetType().GetProperty(entry.Key).SetValue(this, temp, null);
            }
            else
            {
                Debug.LogWarning("Can't load account data, value " + entry.Key + " is missing");
            }
        }
	}


    protected override void gameLoad()
    {
        Load();
        base.gameLoad();
    }
    void Awake()
    {
        Load();
    }

    protected override void gameOver()
    {
        if (Player.Instance.scores > _bestScores)
        {
            _bestScores = Player.Instance.scores;
        }

        _coins += Player.Instance.coins;

        Save();
        base.gameOver();
    }

    void OnApplicationQuit()
    {
        gameOver();
    }

}
