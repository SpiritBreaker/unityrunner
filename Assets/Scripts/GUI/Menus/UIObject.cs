﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIObject : MonoBehaviour {

    public void Awake()
    {

        Game.gameOverNotification += gameOver;
        Game.gameStartNotification += gameStart;
        Game.gamePauseNotification += gamePause;
        Game.gameUnpauseNotification += gameUnpause;
        Game.gameLoadNotification += gameLoad;
    }

    public void Start()
    {
    }

    protected virtual void gameOver()
    {
    }
    protected virtual void gameStart()
    {
    }
    protected virtual void gamePause()
    {
    }
    protected virtual void gameUnpause()
    {
    }
    protected virtual void gameLoad()
    {
    }

    void OnDisable()
    {
        Game.gameOverNotification -= gameOver;
        Game.gameStartNotification -= gameStart;
        Game.gamePauseNotification -= gamePause;
        Game.gameUnpauseNotification -= gameUnpause;
        Game.gameLoadNotification -= gameLoad;
    }
}
