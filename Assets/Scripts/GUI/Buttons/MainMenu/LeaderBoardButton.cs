﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderBoardButton : SimpleButton
{

    public static event ButtonPushed LeaderBoardButtonPushed;

    public override void OnPointerDown()
    {
        if (LeaderBoardButtonPushed != null)
        {
            LeaderBoardButtonPushed();
        }
    }
}
