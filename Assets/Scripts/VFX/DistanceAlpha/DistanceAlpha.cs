﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceAlpha : MonoBehaviour, IDistanceAlpha {

	Material material;
	Color initial_Color;
	Color initial_SColor;
	float initial_Rim;
	float initial_Thr;

	Color _Color;
	Color _SColor;
	float _Rim;
	float _Thr;

	
	[SerializeField]
	float farClip = 300f;
	[SerializeField]
	float nearClip = 50f;

	[SerializeField]
	float distance = 0;

	[SerializeField]
	float inverseLerp = 0;

	void Start()
	{
			material = this.gameObject.GetComponent<Renderer>().material;
			initial_Color = material.GetColor("_Color");
			initial_SColor = material.GetColor("_SColor");
			initial_Rim = material.GetFloat("_Rim");
			initial_Thr = material.GetFloat("_Thr");		

			_Color = new Color(0f, 0f, 0f, 0f);
			_SColor = new Color(0f, 0f, 0f, 0f);
			_Rim = -2;


			material.SetColor("_Color", _Color);
			material.SetColor("_SColor", _SColor);
			material.SetFloat("_Rim", _Rim);

	}

	void Update()
	{
		distance = Vector3.Magnitude(this.transform.position - Camera.main.transform.position);
		inverseLerp = 1 - Mathf.InverseLerp(nearClip, farClip, distance);
		material.SetColor("_Color", Color.Lerp(_Color, initial_Color, inverseLerp));
		material.SetColor("_SColor", Color.Lerp(_SColor, initial_SColor, inverseLerp));
		material.SetFloat("_Rim", Mathf.Lerp(_Rim, initial_Rim, inverseLerp));

	}

	public void setAlpha(float percantage)
	{
		
	}
}
