﻿using UnityEngine;
using System.Collections;
 
#if UNITY_EDITOR
    using UnityEditor;
#endif

 [ExecuteInEditMode]
 public class AlignWithView: MonoBehaviour {
 
     Camera mainCamera;
     public Transform target;
     bool bridge = true;
 
     void Start()
     {
         mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
     }
 
     void OnRenderObject()
     {        if(Event.current.alt)
        {
            Debug.Log("key");
            bridge = !bridge;
        }

#if UNITY_EDITOR

         if (SceneView.lastActiveSceneView != null && SceneView.lastActiveSceneView.camera == Camera.current && bridge) 
         {                
             if (mainCamera != null) 
             {
				 mainCamera.transform.position =  SceneView.lastActiveSceneView.pivot;
				 mainCamera.transform.rotation = SceneView.lastActiveSceneView.rotation;
             }
         }
         
#endif
     }

    void Update()
    {

    }

 }

