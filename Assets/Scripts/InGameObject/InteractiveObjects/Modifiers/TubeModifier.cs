﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TubeModifier : Modifier {

    Vector3 velocity = new Vector3(0.0f, 10.0f, 0.0f);
    float new_height = 10.0f;

    void Start()
    {
        base.Start();
    }

    void Update()
    {
    }

    protected IEnumerator OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("player"))
        {
            Debug.Log("TubeModifier");
            Player.Instance.speed = 3F;
            while (Player.Instance.height < new_height)
            {
                Player.Instance.height= Mathf.SmoothDamp(Player.Instance.height, new_height, ref velocity.y, 1.0f);
                yield return null;
            }
            Destroy(this.gameObject);
        }
    }

    protected override void gameOver()
    {
        Destroy(this.gameObject);
    }
}
