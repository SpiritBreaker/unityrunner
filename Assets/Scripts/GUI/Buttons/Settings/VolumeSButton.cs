﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeSButton : StateButton 
{
    public static event State VolumeStateChanged; 

    public void Awake()
    {
        state = Account.Instance.volume;
    }

    public override void StateChange()
    {
        base.StateChange();
        if (VolumeStateChanged != null)
        {
            VolumeStateChanged(state);
        }
    }
}
