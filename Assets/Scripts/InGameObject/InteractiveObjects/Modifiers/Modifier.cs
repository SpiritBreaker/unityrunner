﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Modifier : InGameObject
{

    //public Material material;
    public bool triggered = false;

    // Use this for initialization
    public void Start()
    {
        base.Start();
        this.GetComponent<Collider>().isTrigger = triggered;
    }

    // Update is called once per frame
    protected void Update()
    {

    }

    protected override void gameOver()
    {
        Destroy(this.gameObject);
    }

}
