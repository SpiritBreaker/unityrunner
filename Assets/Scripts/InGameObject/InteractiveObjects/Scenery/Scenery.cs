﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenery : InteractiveObject
{
    void Start()
    {
        base.Start();
    }

    protected void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("culling_plane"))
        {
            poolManager.Despawn(this.gameObject);
        }
    }
}