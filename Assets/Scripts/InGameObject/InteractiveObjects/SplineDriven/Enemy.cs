﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : ISplineDriven {

    LineRenderer laser;

    public void shot()
    {
        laser = this.gameObject.GetComponent<LineRenderer>();
        laser.enabled = true;
        StartCoroutine(lasiring());
    }

    public void MovedIn()
    {

    }

    public IEnumerator lasiring()
    {
        while(true)
        {
            laser.SetPosition(0, this.transform.position);
            laser.SetPosition(1, this.point.position);
            laser.widthMultiplier = Random.Range(0.05f, 0.30f);
            yield return null;
        }
        yield return null;
    }

    void Start()
    {
        base.Start();
        shot();
    }

}
