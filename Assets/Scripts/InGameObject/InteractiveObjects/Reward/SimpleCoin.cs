﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCoin : Reward {

    void Start()
    {
        base.Start();
    }

    protected void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("player"))
        {
           Player.Instance.coins +=1;
           emmitSound(onEnterSound, 1f);
        }

        base.OnCollisionEnter(other);
    }
}
