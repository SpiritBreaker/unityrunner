﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private float life_time = 0.0f;
    private float limit = 2.0f;

	// Use this for initialization
	void Start () {
		
	}
	// Update is called once per frame
	void Update () {
        life_time += 1 * Time.deltaTime;
        if (life_time >= limit)
        {
            Destroy(this.gameObject);
        }
	}

    void OnCollisionEnter()
    {
        Destroy(this.gameObject);
    }

}
