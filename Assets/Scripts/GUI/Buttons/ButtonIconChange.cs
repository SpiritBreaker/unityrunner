﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonIconChange : MonoBehaviour {

    public Sprite [] types;
    private int type = 0;

    public void Start()
    {
        this.gameObject.GetComponent<Image>().sprite = types[0];
    }

    public void change()
    {
        type = (type + 1) % types.Length;
        this.gameObject.GetComponent<Image>().sprite = types[type];
        Debug.Log(type);
    }
}
