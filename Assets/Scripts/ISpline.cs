﻿using UnityEngine;

namespace Spline 
{
	public struct SplinePoint
    {
        public Vector3 position;
        public Vector3 tangent;
        public Vector3 normal;
        public Vector3 binormal;
    }  

	public interface IApproximation
	{
		float Length(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3);
	}

	public abstract class ISpline
	{
		public float E = 0.01f;
		public double _length = 0F;

		public abstract double length();
    	public abstract int amount_of_points {get; set;}
    	public abstract Vector3 get_tangent(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3);
    	public abstract Vector3 get_world_position(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3);
		public abstract SplinePoint get_point(int i, float t);
		public abstract Transform get_knot(int n);
		public abstract SplinePoint DistanceToPoint(double length);
		public abstract double PointToDistance(int i, float t, Space space = Space.Self);
	}

}



