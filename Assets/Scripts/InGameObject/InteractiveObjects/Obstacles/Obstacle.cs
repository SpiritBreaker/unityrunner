﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : InteractiveObject {

    //Material material;

    void Start()
    {
        base.Start();
        //material = this.gameObject.GetComponent<Renderer>().material;
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("player"))
        {
            Player.Instance.lives -= 1;
            emmitSound(onEnterSound, 1f);
            CullingExplosion.Instance.explode();
            Camera.main.GetComponent<GlitchAnimation>().glitch();
        }

        if(other.gameObject.CompareTag("culling_plane"))
        {
            poolManager.Despawn(this.gameObject);
        }
    }
}
