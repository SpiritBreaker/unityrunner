﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MyMath {
    public static float mod(int x, int m)
    {
        int r = x % m;
        return r < 0 ? r + m : r;
    }

}
