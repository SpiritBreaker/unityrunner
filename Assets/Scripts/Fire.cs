﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class Fire : InGameObject{

	// Use this for initialization
    public GameObject projectile;
 
    public Ray ray;

    private bool enable = false;
	
	// Update is called once per frame
	void Update () 
    {
        if (enable)
        {
            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f);
                ray = Camera.main.ScreenPointToRay(position);
                Shot();
            }
        }
	}

    void Shot()
    {
        GameObject projectile_instance = Instantiate(projectile, transform.position, transform.rotation);
        projectile_instance.AddComponent<Bullet>();
        projectile_instance.transform.forward = this.transform.forward;
        projectile_instance.GetComponent<Rigidbody>().AddForce(ray.direction * 3000);
    }

    protected override void gameStart()
    {
        enable = true;
    }

    protected override void gameOver()
    {
        enable = false;
    }

    protected override void gameLoad()
    {
        enable = true;
    }


}
