﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopButton : SimpleButton
{

    public static event ButtonPushed ShopButtonPushed;

    public override void OnPointerDown()
    {
        if (ShopButtonPushed != null)
        {
            ShopButtonPushed();
        }
    }
}
