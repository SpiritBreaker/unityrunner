﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlButton : SimpleButton {

    public delegate void PlayerController(float input);
    public static event PlayerController ControllerButton;

    public void PointerDown(float i)
    {
        ControllerButton(i);
    }

    public void PointerUp()
    {
        ControllerButton(0);
    }
}
