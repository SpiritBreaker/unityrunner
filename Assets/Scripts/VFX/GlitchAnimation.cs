﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlitchAnimation : MonoBehaviour {


	float glitchTime = 0.5f;

	public IEnumerator run()
	{
		Camera.main.GetComponent<Kino.AnalogGlitch>().enabled = true;

		Camera.main.GetComponent<Kino.AnalogGlitch>().scanLineJitter = 0.5f;
		Camera.main.GetComponent<Kino.AnalogGlitch>().verticalJump = 0.5f;
		Camera.main.GetComponent<Kino.AnalogGlitch>().horizontalShake = 0.5f;
		Camera.main.GetComponent<Kino.AnalogGlitch>().colorDrift = 0.5f;

		while(true)
		{
			if(this.glitchTime > 0.01f)
			{
				this.glitchTime -= 0.01f;
				Camera.main.GetComponent<Kino.AnalogGlitch>().scanLineJitter = glitchTime;
				Camera.main.GetComponent<Kino.AnalogGlitch>().verticalJump = glitchTime;
				Camera.main.GetComponent<Kino.AnalogGlitch>().horizontalShake = glitchTime;
				Camera.main.GetComponent<Kino.AnalogGlitch>().colorDrift = glitchTime;
				yield return null;
			}
			else 
			{
				this.glitchTime = 0.5f;
				Camera.main.GetComponent<Kino.AnalogGlitch>().enabled = false;
				yield break;
			}
		}
	}

	public void glitch()
	{
		StartCoroutine(run());
	}
}
