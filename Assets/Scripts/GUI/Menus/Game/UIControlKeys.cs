﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControlKeys : UIObject
{

    public Canvas[] canvases;

    private void hideElements()
    {
        foreach (Canvas canvas in canvases)
        {
            canvas.enabled = false;
        }
    }
    private void showElements()
    {
        foreach (Canvas canvas in canvases)
        {
            canvas.enabled = true;
        }
    }

    protected override void gameStart()
    {
        showElements();
    }

    protected override void gamePause()
    {
        hideElements();
    }

    protected override void gameLoad()
    {
        hideElements();
    }

    protected override void gameOver()
    {
        hideElements();
    }

    protected override void gameUnpause()
    {
        showElements();
    }
}
