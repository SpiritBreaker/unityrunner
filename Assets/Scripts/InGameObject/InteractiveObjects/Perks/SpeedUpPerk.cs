﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUpPerk : Perk {

	
    IEnumerator SpeedUp()
    {
        float FOV = Camera.main.fieldOfView;
        StartCoroutine(Decorators.Tween((x) => Player.Instance.speed = x, Player.Instance.speed, Player.Instance.speed + 2, 0.3f));
        StartCoroutine(Decorators.Tween((x) => Camera.main.fieldOfView = x, FOV, FOV + 40, 0.3f));
        yield return new WaitForSeconds(2f);
        
        StartCoroutine(Decorators.Tween((x) => Player.Instance.speed = x, Player.Instance.speed, Player.Instance.speed - 2, 2f));
        StartCoroutine(Decorators.Tween((x) => Camera.main.fieldOfView = x, FOV + 40, FOV, 2f));
        
        yield return new WaitForSeconds(3f);
        poolManager.Despawn(this.gameObject);
    }

    protected void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("player"))
        {
            StartCoroutine(SpeedUp());
            emmitSound(onEnterSound, 1f);
        }

        //base.OnCollisionEnter(other);
    }
}
