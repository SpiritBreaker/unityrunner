﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ScreenSpaceEffect : MonoBehaviour {

	#region Variables
	public Shader curShader;
	public float depthPower = 1.0f;
	public Material curMaterial;


    const int iterations = 3;
	RenderTexture[] _blurBuffer = new RenderTexture[iterations];
	RenderTexture[] _upSampleBuffer = new RenderTexture[iterations];
	#endregion

	#region Properties
	Material material
	{
		get
		{
			if (curMaterial == null) 
			{
				curMaterial = new Material (curShader);
				curMaterial.hideFlags = HideFlags.HideAndDontSave;
			}

			return curMaterial;
		}
	}
	#endregion

	void Start()
	{
		if (!SystemInfo.supportsImageEffects) {
			enabled = false;
			return;
		}

		if (!curShader && !curShader.isSupported) {
			enabled = false;
		}
		Camera.main.depthTextureMode = DepthTextureMode.Depth;
		material.SetFloat ("_DepthPower", depthPower);
	}

	void OnRenderImage(RenderTexture sourceTexture, RenderTexture destTexture)
	{

		var useRGBM = Application.isMobilePlatform;

            // blur buffer format
            var rtFormat = useRGBM ?
                RenderTextureFormat.Default : RenderTextureFormat.DefaultHDR;

		var prefiltered = RenderTexture.GetTemporary(sourceTexture.width, sourceTexture.height, 0, rtFormat);
	    Graphics.Blit(sourceTexture, prefiltered);
		
		var last = prefiltered;
		for (int i = 0; i < iterations; i++)
		{
 			_blurBuffer[i] = RenderTexture.GetTemporary(last.width/2, last.height/2,  0, rtFormat);
			Graphics.Blit (last, _blurBuffer[i]);	
			last = _blurBuffer[i];	
		}

		for (int i = 0; i < iterations; i++)
		{
 			_upSampleBuffer[i] = RenderTexture.GetTemporary(last.width*2, last.height*2, 0, rtFormat);
			Graphics.Blit (last, _upSampleBuffer[i]);	
			last = _upSampleBuffer[i];	
		}
        
	    material.SetTexture("_MainTex", sourceTexture);
		material.SetTexture("_BlendTex", last);
		Graphics.Blit (sourceTexture, destTexture, material);


        for (int i = 0; i < iterations; i++)
            {
                if (_blurBuffer[i] != null)
                    RenderTexture.ReleaseTemporary(_blurBuffer[i]);

                if (_upSampleBuffer[i] != null)
                    RenderTexture.ReleaseTemporary(_upSampleBuffer[i]);

                _blurBuffer[i] = null;
                _upSampleBuffer[i] = null;
            }

		RenderTexture.ReleaseTemporary(prefiltered);
	}

	void Update()
	{
		depthPower = Mathf.Clamp (depthPower, 0, 5);
	}

	void OnDisable()
	{
		if (curMaterial) {
			//DestroyImmediate (curMaterial);
		}
	}

}
