﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameShopButton : SimpleButton
{
    public static event ButtonPushed InGameShopButtonPushed;
    public override void OnPointerDown()
    {
        if (InGameShopButtonPushed != null)
        {
            InGameShopButtonPushed();
        }
    }
}
