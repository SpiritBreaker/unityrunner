﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spline;

public class InteractiveObjectsFactory : InGameObject, IFactory {

    PoolManager poolManager;

    [SerializeField]
    public double nearDistance = 10.0f;
    [SerializeField]
    public double farDistance = 90.0f;
    [SerializeField]
    public double step = 10f;

    public List<GameObject> productionLines = new List<GameObject>();
    IProductionLine activeProductionLine;

    public bool isPaused = false;
    public IEnumerator coroutine;
    public ISpline spline;

    IFactory factory;

    public IEnumerator InstantiateOverTime()
    {
        double pos = spline.PointToDistance(Player.Instance.position, Player.Instance.segment_position);        
        GameObject newObject = activeProductionLine.Create(poolManager, spline, pos + nearDistance, pos + farDistance + nearDistance);
        
        double delta;
        double snapshot = Player.Instance.traveledDistance;

        yield return null;

        while (true)
        {
            double current_pos =  Player.Instance.traveledDistance;
            delta = Mathf.Abs((float)(current_pos - snapshot));
            if (delta > farDistance)
            {
                    double begin = current_pos + farDistance + nearDistance;
                    double end = begin + farDistance;
                    newObject = activeProductionLine.Create(poolManager, spline, begin, end);
                    snapshot = current_pos;  
            }
            
            yield return null;
        }    
    }

    void Start()
    {
        base.Start();
        if (poolManager == null)
        {
            poolManager = gameObject.AddComponent<PoolManager>();
        }
        spline = Game.spline;
        activeProductionLine = productionLines[0].GetComponent<IProductionLine>() as IProductionLine;
    }


    void Update()
    {
        activeProductionLine = productionLines[gameObject.GetComponent<Randomness>().random_value].GetComponent<IProductionLine>() as IProductionLine;
    }

    protected override void gameStart()
    {
        isPaused = false;
        coroutine = InstantiateOverTime();
        StartCoroutine(coroutine);
        base.gameStart();
    }

    protected override void gamePause()
    {
        isPaused = true;
        base.gamePause();
    }

    protected override void gameUnpause()
    {
        isPaused = false;
        base.gameUnpause();
    }

    protected override void gameOver()
    {
        isPaused = true;
        StopCoroutine(coroutine);
        base.gameOver();
    }

}
