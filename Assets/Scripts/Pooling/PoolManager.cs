﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PoolManager : InGameObject {

    [SerializeField]
    public int active_pool = 0;
    [SerializeField]
    public int deactive_pool = 0;

    public bool isPaused = false;
    public bool stop = false;

    HashPooling hashPooling = new HashPooling();

    public void Update()
    {
        if (hashPooling.ActivePools != null) active_pool = hashPooling.ActivePools.First().Value.buffer.Count;
        if (hashPooling.DeactivePools  != null) deactive_pool = hashPooling.DeactivePools.First().Value.buffer.Count;
    }
    

    public IEnumerator DespawnPools()
    {
        if (hashPooling.ActivePools != null)
        {
            foreach (KeyValuePair<GameObject, HashPool> ActivePool in hashPooling.ActivePools)
            {
                GameObject prefab = ActivePool.Value.prefab;

                hashPooling.DeactivePools[prefab].buffer.UnionWith(
                    hashPooling.ActivePools[prefab].buffer);

                foreach (GameObject obj in hashPooling.DeactivePools[prefab].buffer)
                {
                    obj.SetActive(false);
                }

                ActivePool.Value.buffer.Clear();
                yield return null;
            }
        }
    }

    public GameObject Spawn(GameObject obj)
    {
        return hashPooling.Spawn(obj);
    }

    public GameObject Despawn(GameObject obj)
    {
        return hashPooling.Despawn(obj);
    }

    protected override void gameStart()
    {
        base.gameStart();
    }

    protected override void gameOver()
    {
        StartCoroutine(DespawnPools());
        base.gameOver();
    }

    protected override void gamePause()
    {
        base.gamePause();
    }


}
