﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton : SimpleButton {

    public static event ButtonPushed PlayButtonPushed;

    public override void OnPointerDown()
    {
        if (PlayButtonPushed != null)
        {
            PlayButtonPushed();
        }
    }
}
