﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/DepthShader" {
	Properties {
		_MainTex("Base(RGB)", 2D) = "white" {}
		_BlendTex ("Blend(RGB)", 2D) = "white" {}

		_DepthPower("Depth Power", Range(1, 5)) = 1
		_DistanceOffset("Distance Offset", Range(0, 5)) = 0
		_Density("Density", Range(0, 0.1)) = 0
		_SkyTint ("-", Color) = (.5, .5, .5, .5)
		[NoScaleOffset] _SkyCubemap ("-", Cube) = "" {}
	}



	SubShader
	{
		Cull Off 		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform sampler2D _BlendTex;
			fixed _DepthPower;
			float _DistanceOffset;
			float _Density;
			half4 _SkyTint;
			float2 _MainTex_TexelSize;
			sampler2D _CameraDepthTexture;
			samplerCUBE _SkyCubemap;


    		struct v2f
    		{
        		float4 pos : SV_POSITION;
        		float2 uv : TEXCOORD0;
				float4 scrPos : TEXCOORD1;
				float3 ray : TEXCOORD2;
    		};

			    // Applies one of standard fog formulas, given fog coordinate (i.e. distance)
    half ComputeFogFactor(float coord)
    {
        float fog = 0.0;
    #if FOG_LINEAR
        // factor = (end-z)/(end-start) = z * (-1/(end-start)) + (end/(end-start))
        fog = coord * _LinearGrad + _LinearOffs;
    #elif FOG_EXP
        // factor = exp(-density*z)
        fog = _Density * coord;
        fog = exp2(-fog);
    #else // FOG_EXP2
        // factor = exp(-(density*z)^2)
        fog = _Density * coord;
        fog = exp2(-fog * fog);
    #endif
        return saturate(fog);
    }


    // Distance-based fog
    float ComputeDistance(float3 ray, float depth)
    {
        float dist;
    #if RADIAL_DIST
        dist = length(ray * depth);
    #else
        dist = depth * _ProjectionParams.z;
    #endif
        // Built-in fog starts at near plane, so match that by
        // subtracting the near value. Not a perfect approximation
        // if near plane is very large, but good enough.
        dist -= _ProjectionParams.y;
        return dist;
    }


			v2f vert(appdata_full vertIn)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(vertIn.vertex);
				o.uv = vertIn.texcoord.xy;
				o.scrPos = ComputeScreenPos(o.pos);
                o.ray = vertIn.texcoord1.xyz;
                return o;
            }


			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 colorBuffer = tex2D(_MainTex, i.uv);
				fixed4 blendTex = tex2D(_BlendTex, i.uv);				
				float depthValue = Linear01Depth (tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)).r);

				float g = ComputeDistance(i.ray,depthValue) - _DistanceOffset;
        		half fog = ComputeFogFactor(max(0.0, g));

				half3 skyColor = texCUBE(_SkyCubemap, i.ray);
			
				return lerp(blendTex, colorBuffer, fog);
			}
			ENDCG

		}
	}

}
