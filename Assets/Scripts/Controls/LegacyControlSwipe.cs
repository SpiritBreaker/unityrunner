﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;



public class LegacyControlSwipe : IControl {

	Vector3 mousePressedPosition;
	Vector3 mousePosition;
	Vector3 mouseReleasedPosition;

	Vector3 mousePreviousPosition;
	float beginAngle;

    float velocity;
    Coroutine velocityCoroutine;
    Coroutine easyInOutCoroutine;

    float delta;

	private float beginTime;

	// Use this for initialization
	void Start () {
	}

	void MousePressed()
	{
		acceleration.x = 0f;
		beginAngle = Player.Instance.angle;
		mousePressedPosition = Input.mousePosition;
		beginTime = Time.time;
	}

	void MouseMove()
	{

        delta = (Camera.main.ScreenToViewportPoint(Input.mousePosition) - mousePreviousPosition).x;

		float angle = Camera.main.ScreenToViewportPoint(mousePressedPosition).x - Camera.main.ScreenToViewportPoint(Input.mousePosition).x;
		acceleration.angle = beginAngle + (angle * 100f); 

        mousePreviousPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
	}

    IEnumerator easyInOut()
    {
        float x = 0.0f;
        while (velocity > 0.001f)
        {
            velocity = 10.0f*(Mathf.Pow(0.7f, x));
            x+=1f * Time.deltaTime;
            yield return null;
        }
        yield break;

    }


    IEnumerator velocityAngle()
    {
        if (acceleration.x > 0)
        {
            while (acceleration.x > 0.0f)
            {
                acceleration.angle -= 30f * acceleration.x * Time.deltaTime;
                acceleration.x -= (10f - velocity) * Time.deltaTime;
                yield return null;
            }
            yield break;
        }
        else{
            while (acceleration.x < 0.0f)
            {
                acceleration.angle -= 30f * acceleration.x * Time.deltaTime;
                acceleration.x += (10f - velocity) * Time.deltaTime;
                yield return null;
            }
            yield break;
            }
    }

	void MouseReleased()
	{
	
        acceleration.x = delta * 100f;
        velocity = 10f;

        if (velocityCoroutine !=null )
        {
            StopCoroutine(velocityCoroutine);
            velocityCoroutine = StartCoroutine(velocityAngle());

            StopCoroutine(easyInOutCoroutine);
            easyInOutCoroutine = StartCoroutine(easyInOut());
        }
        else
        {
            velocityCoroutine = StartCoroutine(velocityAngle());
            easyInOutCoroutine = StartCoroutine(easyInOut());
        }


		
	}

	// Update is called once per frame
	void Update () {
		if (!EventSystem.current.currentSelectedGameObject)
		{
			if (Input.GetMouseButtonDown(0))
			{
				MousePressed();
			}

			if (Input.GetMouseButton(0))
			{
				MouseMove();
			}	

			if (Input.GetMouseButtonUp(0))
			{
				MouseReleased();
			}
		}
	}
}