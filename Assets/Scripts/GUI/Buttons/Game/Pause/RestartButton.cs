﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartButton : SimpleButton
{
    public static event ButtonPushed RestartButtonPushed;
    public override void OnPointerDown()
    {
        if (RestartButtonPushed != null)
        {
            RestartButtonPushed();
        }
    }
}
