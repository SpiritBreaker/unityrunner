﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(UIHealthBar))]
public class UIHealthBarEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        UIHealthBar myScript = (UIHealthBar)target;
        if(GUILayout.Button("Build Object"))
        {
            myScript.dropDown(30);
        }
    }
}

#endif