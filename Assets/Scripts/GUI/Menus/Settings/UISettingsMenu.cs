﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISettingsMenu : UIObject
{

    void Awake()
    {
        base.Awake();
        SettingsButton.SettingsButtonPushed += showElements;
        SettingsBackButton.SettingsBackButtonPushed += hideElements;
    }

    private void hideElements()
    {
        this.gameObject.GetComponent<Canvas>().enabled = false;
    }
    private void showElements()
    {
        this.gameObject.GetComponent<Canvas>().enabled = true;
    }


    protected override void gameStart()
    {
    }

    protected override void gameOver()
    {
    }
    protected override void gamePause()
    {
    }

    protected override void gameLoad()
    {

        hideElements();
    }

    protected override void gameUnpause()
    {

    }

}
