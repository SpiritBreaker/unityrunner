﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class TestRenderImage : MonoBehaviour {

	#region Variables
	public Shader curShader;
	public float depthPower = 1.0f;
	public Material curMaterial;
	#endregion

	#region Properties
	Material material
	{
		get
		{
			if (curMaterial == null) 
			{
				curMaterial = new Material (curShader);
				curMaterial.hideFlags = HideFlags.HideAndDontSave;
			}

			return curMaterial;
		}
	}
	#endregion

	void Start()
	{

		Camera.main.depthTextureMode = DepthTextureMode.Depth;
		depthPower = Mathf.Clamp (depthPower, 0, 5);

		if (!SystemInfo.supportsImageEffects) {
			enabled = false;
			return;
		}

		if (!curShader && !curShader.isSupported) {
			enabled = false;
		}
	}
	
	[ImageEffectOpaque]
	void OnRenderImage(RenderTexture sourceTexture, RenderTexture destTexture)
	{

			material.SetFloat ("_DepthPower", depthPower);
			Graphics.Blit (sourceTexture, destTexture, material);


		  // Calculate vectors towards frustum corners.
            var cam = GetComponent<Camera>();
            var camtr = cam.transform;
            var camNear = cam.nearClipPlane;
            var camFar = cam.farClipPlane;

            var tanHalfFov = Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad / 2);
            var toRight = camtr.right * camNear * tanHalfFov * cam.aspect;
            var toTop = camtr.up * camNear * tanHalfFov;

            var v_tl = camtr.forward * camNear - toRight + toTop;
            var v_tr = camtr.forward * camNear + toRight + toTop;
            var v_br = camtr.forward * camNear + toRight - toTop;
            var v_bl = camtr.forward * camNear - toRight - toTop;

            var v_s = v_tl.magnitude * camFar / camNear;

            // Draw screen quad.
            RenderTexture.active = destTexture;


            material.SetTexture("_MainTex", sourceTexture);
            material.SetPass(0);

            GL.PushMatrix();
            GL.LoadOrtho();
            GL.Begin(GL.QUADS);

            GL.MultiTexCoord2(0, 0, 0);
            GL.MultiTexCoord(1, v_bl.normalized * v_s);
            GL.Vertex3(0, 0, 0.1f);

            GL.MultiTexCoord2(0, 1, 0);
            GL.MultiTexCoord(1, v_br.normalized * v_s);
            GL.Vertex3(1, 0, 0.1f);

            GL.MultiTexCoord2(0, 1, 1);
            GL.MultiTexCoord(1, v_tr.normalized * v_s);
            GL.Vertex3(1, 1, 0.1f);

            GL.MultiTexCoord2(0, 0, 1);
            GL.MultiTexCoord(1, v_tl.normalized * v_s);
            GL.Vertex3(0, 1, 0.1f);

            GL.End();
            GL.PopMatrix();
		 

	}

	void Update()
	{

	}

	void OnDisable()
	{
		if (curMaterial) {
			//DestroyImmediate (curMaterial);
		}
	}

}
