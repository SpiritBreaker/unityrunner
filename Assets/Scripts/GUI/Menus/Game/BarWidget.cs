﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

//The base class of Bar system
public abstract class BarWidget
{
    public Canvas canvas;
    public RectTransform rectTransform;
    public GameObject obj;
    public Mask mask;

    public BarWidget(Canvas canvas)
    {
        if (canvas == null)
        {
            throw new ArgumentNullException("Canvas are not set");
        }
        this.canvas = canvas;
        this.rectTransform = canvas.gameObject.GetComponent<RectTransform>();
        this.obj = canvas.gameObject;
        this.mask = canvas.gameObject.GetComponent<Mask>();
    }

    public float width
    {
        get { return rectTransform.sizeDelta.x * rectTransform.localScale.x; }
        set { rectTransform.sizeDelta = new Vector2(value, height); }
    }
    public float height
    {
        get { return rectTransform.sizeDelta.y * rectTransform.localScale.y; }
        set { rectTransform.sizeDelta = new Vector2(width, value); }
    }

    public Vector2 size
    {
        get { return new Vector2(width, height); }
        set { rectTransform.sizeDelta = value; }
    }

    public Vector3 scale
    {
        get {return rectTransform.localScale;}
        set {rectTransform.localScale = value;}
    }

    public Quaternion rotation
    {
        get {return rectTransform.localRotation;}
        set {rectTransform.localRotation = value;}
    }

    public Vector3 position
    {
        get { return position; }
        set { rectTransform.localPosition = value; }
    }
}