﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButton : SimpleButton
{
    public static event ButtonPushed ExitButtonPushed;
    public override void OnPointerDown()
    {
        if (ExitButtonPushed != null)
        {
            ExitButtonPushed();
        }
    }
}
