﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : InGameObject {

    #region Singleton
    private static Player _instance;
    public static Player Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Player>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("Player");
                    _instance = container.AddComponent<Player>();
                }
            }

            return _instance;
        }
    }
    #endregion

    public delegate void ChangeLives(int input);
    public static event ChangeLives livesChangeNotification;

    [SerializeField]
    private int _lives = 2;

    [SerializeField]
    private float _speed = 0.0f;

    [SerializeField]
    private float _angle = 0.0f;
    
    [SerializeField]
    private float _height = 3.0f;

    [SerializeField]
    private int _position = 0;

    [SerializeField]
    private float _segment_position = 0.0f;

    [SerializeField]
    private int _scores = 0;

    [SerializeField]
    private bool _freeze_scores = false;

    [SerializeField]
    private int _coins = 0;

    [SerializeField]
    public double traveledDistance = 0F;

    void Update()
    {

    }

    public float angle
    {
        get
        {
            return _angle;
        }
        set
        {
            _angle = value;
        }
    }

    public int position
    {
        get
        {
            return _position;
        }
        set
        {
            _position = value;
        }
    }

    public float segment_position
    {
        get
        {
            return _segment_position;
        }
        set
        {
            _segment_position = value;
        }
    }

    public int coins { 
        get 
        { 
            return _coins; 
        } 
        set 
        { 
            _coins = value; 
        } 
    }

    public int lives
    {
        get 
        {
            return _lives;
        }

        set
        {
            _lives = value;
            livesChangeNotification(_lives);
        }
    }

    public float speed
    {
        set
        {
            _speed = value;
        }
        get 
        {
            return _speed;
        }
    }


    public float height
    {
        get
        {
            return _height;
        }

        set
        {
            _height = value;
        }
    }



    public int scores
    {
        set 
        {
            _scores = value;
        }

        get
        {
            return _scores;
        }
    }


    public void pick_up_perk(int i)
    {

    }

    protected override void gameOver()
    {
        StartCoroutine(Decorators.Tween((x) => speed = x, speed, 0.5f, 0.5f));
        traveledDistance = 0F;
    }

    protected override void gameStart()
    {
        StartCoroutine(Decorators.Tween((x) => speed = x, 0.5f, 3f, 0.5f));
        _lives = 3;
    }

    protected override void gameLoad()
    {
        _speed = 0.5f;
    }


}
