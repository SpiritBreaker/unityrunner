﻿
// compile with: /unsafe
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decorators : MonoBehaviour {


    //float
    static public IEnumerator Tween(System.Action<float> var,
           float aa, float zz, float duration)
    {
        float sT = Time.time;
        float eT = sT + duration;

        while (Time.time < eT)
        {
            float t = (Time.time - sT) / duration;
            var(Mathf.SmoothStep(aa, zz, t));
            yield return null;
        }

        var(zz);
    }
}

