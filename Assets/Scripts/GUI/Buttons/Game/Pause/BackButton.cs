﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackButton : SimpleButton
{
    public static event ButtonPushed BackButtonPushed;
    public override void OnPointerDown()
    {
        if (BackButtonPushed != null)
        {
            BackButtonPushed();
        }
    }
}
