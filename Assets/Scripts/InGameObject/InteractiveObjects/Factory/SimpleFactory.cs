﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spline;

public class SimpleFactory : InGameObject, IFactory {

    IFactory factory;
    public GameObject obj;
    PoolManager poolManager;
    ISpline spline;

    public int density_x = 10;
    public int density_y = 10;

    [SerializeField]
    public double nearDistance = 10.0f;
    [SerializeField]
    public double farDistance = 90.0f;
    [SerializeField]
    public double step = 10f;

    //public IEnumerator InstantiateOverTime()
    //{
    //    double pos = spline.PointToDistance(Player.Instance.position, Player.Instance.segment_position);
    //    GameObject newObject = activeProductionLine.Create(poolManager, spline, pos + nearDistance, pos + farDistance + nearDistance);

    //    double delta;
    //    double snapshot = Player.Instance.traveledDistance;

    //    yield return null;

    //    while (true)
    //    {
    //        double current_pos = Player.Instance.traveledDistance;
    //        delta = Mathf.Abs((float)(current_pos - snapshot));
    //        if (delta > farDistance)
    //        {
    //            double begin = current_pos + farDistance + nearDistance;
    //            double end = begin + farDistance;
    //            newObject = activeProductionLine.Create(poolManager, spline, begin, end);
    //            snapshot = current_pos;
    //        }

    //        yield return null;
    //    }
    //}

    void Start()
    {
        base.Start();
        if (poolManager == null)
        {
            poolManager = gameObject.AddComponent<PoolManager>();
        }
        spline = Game.spline;
    }

    void Update()
    {

    }
}
