﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UIHealthBar : UIObject {
	
	

	private HealthBar healthBar;

	[SerializeField]
	public ParticleSystem HealthPaticles;
	[SerializeField]
	public ParticleSystem WealdingParticles;

	[SerializeField]
	public Sprite sprite;
	[SerializeField]
	public Canvas canvas;
	[SerializeField]
	public Material material;
	[SerializeField]
	public RectTransform canvasRect;
	[SerializeField]
	public float width;
	[SerializeField]
	public float height;

	[SerializeField]
	public int numbers;

	[SerializeField]
	public int dropPercent = 1;

	[SerializeField]
	public int regenSpeed = 1;

	[SerializeField]
	public Vector3 barCorner;


	public void dropDown(int percent)
	{
        StartCoroutine(healthBar.drop(percent));
		HealthPaticles.Play();
		WealdingParticles.Play();
		Debug.Log("dropdown");
	}

	// Use this for initialization
	void Start () 
	{
		HealthPaticles.Stop();
		WealdingParticles.Stop();
        canvas = this.gameObject.GetComponent<Canvas>();
        healthBar = new HealthBar(canvas, numbers, 2f);
        if (material != null)
        {
            foreach (HealthUnit unit in healthBar.healthUnits)
            {
                unit.image.material = material;
            }
        }
		
		if (sprite != null)
        {
            foreach (HealthUnit unit in healthBar.healthUnits)
            {
                unit.image.sprite = sprite;
            }
        }
        StartCoroutine(healthBar.regain());
	}
	
	// Update is called once per frame
	void Update () 
	{
		
        width = healthBar.width;
        height = healthBar.height;
		Vector3[] v = new Vector3[4];
		healthBar.rectTransform.GetWorldCorners(v);
		HealthPaticles.transform.position= ((v[2] + v[3])/2) + new Vector3(0f, -1f, -5f);
		WealdingParticles.transform.position= v[2] + new Vector3(0f, -1f, -1f);
	}
}
