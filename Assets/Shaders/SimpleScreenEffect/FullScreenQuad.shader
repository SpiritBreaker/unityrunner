﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/FullScreenQuad" {
	Properties {
		_MainTex("Base(RGB)", 2D) = "white" {}
	}

	SubShader
	{
		Cull Off 		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
    		struct v2f
    		{
        		float4 pos : SV_POSITION;
        		float2 uv : TEXCOORD0;
				float3 uv2 : TEXCOORD2;
    		};

			v2f vert(appdata_full vertIn)
            {
                v2f o;
                o.pos = mul(_WorldSpaceCameraPos, vertIn.vertex);
				o.uv = vertIn.texcoord.xy;
                o.uv2 = vertIn.texcoord1.xyz;
                return o;
            }

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 colorBuffer = tex2D(_MainTex, i.uv2);

				//half3 skyColor = texCUBE(_SkyCubemap, i.ray), _SkyCubemap_HDR);
				
				return  float4(i.uv2, 1.0);
			}
			ENDCG

		}
	}

}
