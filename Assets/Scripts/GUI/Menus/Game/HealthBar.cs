﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Util
{
    public static Rect RectTransformToScreenSpace(RectTransform transform)
    {
        Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
        return new Rect((Vector2)transform.position - (size * 0.5f), size);
    }
}

public class HealthBar : BarWidget
{
    float margin;
    float initialWidth;
    
    public List<HealthUnit> healthUnits;

    Vector2 anchorMin = new Vector2(1, 1f);
    Vector2 anchorMax = new Vector2(1, 1f);
    Vector2 pivot = new Vector2(0, 0.5f);

    RectTransform parentRect;

    public HealthBar(Canvas canvas, int n, float margin)
        : base(canvas)
    {
        parentRect = canvas.transform.parent.GetComponent<RectTransform>();

        healthUnits = new List<HealthUnit>();

        this.margin = margin;

        float squadWidth = ((this.rectTransform.rect.width) - (margin * n)) / n;
        float squadHeight = ((this.rectTransform.rect.height)  - (margin * 2));

        initialWidth = width;

        this.rectTransform.anchorMin = this.anchorMin;
        this.rectTransform.anchorMax = this.anchorMax;
        this.rectTransform.pivot = this.pivot;

        Vector3 begin = new Vector3((squadWidth / 2) + margin, 0f, 0f);
        float step = squadWidth;

        for (int i = 0; i < n; i++)
        {
            HealthUnit healthUnit = new HealthUnit(canvas);
            healthUnit.size = new Vector2(squadWidth, squadHeight);
            healthUnit.scale = new Vector3(1f, 1f, 1f);
            healthUnit.rotation = new Quaternion(0f, 0f, 0f, 1f);
            healthUnit.position = begin;
            begin.x += step + margin;
            healthUnits.Add(healthUnit);
        }

        healthUnits.Reverse();
    }

    public IEnumerator regain()
    {
        while (true)
        {
            if (width < initialWidth)
            {
                width += 5f * Time.deltaTime;
                yield return null;
            }
            yield return null;
        }
    }

    public IEnumerator drop(int percent)
    {
        width = width - ((initialWidth + (margin * healthUnits.Count)) / 3f);
        yield break;
        //yield return null;
    }

    public IEnumerator freeze()
    {
        yield return null;
    }
}

public class HealthUnit : BarWidget
{
    public Image image;
    Vector2 anchorMin = new Vector2(0, 0.5f);
    Vector2 anchorMax = new Vector2(0, 0.5f);


    public HealthUnit(Canvas canvas = null)
        : base(canvas)
    {
        this.obj = new GameObject("squad", typeof(RectTransform));
        this.rectTransform = this.obj.GetComponent<RectTransform>();
        this.rectTransform.anchorMin = this.anchorMin;
        this.rectTransform.anchorMax = this.anchorMax;
        obj.transform.parent = canvas.transform;
        image = obj.AddComponent<Image>();
    }
}

